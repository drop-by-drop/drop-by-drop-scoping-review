TY  - JOUR
ID  - 2023-43765-001
AN  - 2023-43765-001
AU  - Mylonas, Georgios
AU  - Hofstaetter, Joerg
AU  - Giannakos, Michail
AU  - Friedl, Andreas
AU  - Koulouris, Pavlos
T1  - Playful interventions for sustainability awareness in educational environments: A longitudinal, large-scale study in three countries
JF  - International Journal of Child-Computer Interaction
JO  - International Journal of Child-Computer Interaction
JA  - Int J Child Comput Interact
Y1  - 2023/03//
VL  - 35
SP  - 1
EP  - 17
PB  - Elsevier Science
SN  - 2212-8689
SN  - 2212-8697
AD  - Giannakos, Michail
N1  - Accession Number: 2023-43765-001. Partial author list: First Author & Affiliation: Mylonas, Georgios; Industrial Systems Institute, Athena Research and Innovation Center, Patras, Greece. Release Date: 20230413. Publication Type: Journal (0100), Peer Reviewed Journal (0110). Format Covered: Electronic. Document Type: Journal Article. Language: EnglishMajor Descriptor: Awareness; School Environment; Sustainability. Minor Descriptor: Communities. Classification: Classroom Dynamics & Student Adjustment & Attitudes (3560). Population: Human (10); Male (30); Female (40). Location: Greece; Italy; Sweden. Age Group: Childhood (birth-12 yrs) (100); School Age (6-12 yrs) (180); Adolescence (13-17 yrs) (200). Methodology: Empirical Study; Longitudinal Study; Quantitative Study. References Available: Y. Page Count: 17. ArtID: 100562. Issue Publication Date: Mar, 2023. Publication History: First Posted Date: Dec 29, 2022; Accepted Date: Dec 19, 2022; Revised Date: Dec 10, 2022; First Submitted Date: Jan 8, 2022. Copyright Statement: Published by Elsevier B.V. This is an open access article under the CC BY license (http://creativecommons.org/licenses/by/4.0/). The Author(s). 2022. 
AB  - With the United Nations (UN) Sustainable Development Goals and national commitments requiring effective implementation of evidence-based innovations, we are witnessing a growing number of related interventions in educational environments based on digitalization and gamification. In particular, serious games and playful activities are being used to empower children’s awareness and reflection on issues related to climate change and sustainability. In this work, we discuss such approaches through the lens of our own experience with an intervention targeting school environments. Our study is based on a playful web application that focuses on sustainability awareness and energy-related aspects, the GAIA Challenge, which was used in over 25 schools in 3 countries, resulting in 3762 registered users. We present a longitudinal study on the use of GAIA Challenge focusing on children’s engagement, rate of completion of the content on offer, and overall reaction by the school communities involved in large-scale trials, complemented with insights from surveys answered by 723 students and 32 educators. Our results showed that up to 20% of the children completed all content available in the Challenge, when gameplay sessions were combined with strong community competition activities. The children also reported increased awareness of related sustainability issues. Our findings demonstrate that a simple playful experience can yield good results within educational environments, by taking into consideration typical school constraints, integrating it to the daily activities of schools and placing it within their strategy. (PsycInfo Database Record (c) 2023 APA, all rights reserved)
KW  - Gamification
KW  - Sustainability education
KW  - Sustainability in schools
KW  - Sustainability
KW  - Energy saving
KW  - IoT
KW  - Awareness
KW  - School Environment
KW  - Sustainability
KW  - Communities
U1  - Sponsor: EC, EASME, Europe. Grant: 696029. Other Details: EU research project Green Awareness In Action, Under H2020. Recipients: No recipient indicated
DO  - 10.1016/j.ijcci.2022.100562
UR  - https://login.ezproxy.elib11.ub.unimaas.nl/login?url=https://search.ebscohost.com/login.aspx?direct=true&db=psyh&AN=2023-43765-001&site=ehost-live&scope=site
UR  - michailg@ntnu.no
DP  - EBSCOhost
DB  - psyh
ER  - 

TY  - JOUR
ID  - 2019-37840-006
AN  - 2019-37840-006
AU  - Mylonas, Georgios
AU  - Amaxilatis, Dimitrios
AU  - Pocero, Lidia
AU  - Markelis, Iraklis
AU  - Hofstaetter, Joerg
AU  - Koulouris, Pavlos
T1  - An educational IoT lab kit and tools for energy awareness in European schools
JF  - International Journal of Child-Computer Interaction
JO  - International Journal of Child-Computer Interaction
JA  - Int J Child Comput Interact
Y1  - 2019/06//
VL  - 20
SP  - 43
EP  - 53
PB  - Elsevier Science
SN  - 2212-8689
SN  - 2212-8697
AD  - Mylonas, Georgios
N1  - Accession Number: 2019-37840-006. Partial author list: First Author & Affiliation: Mylonas, Georgios; Computer Technology Institute and Press 'Diophantus', Patras, Greece. Release Date: 20191107. Correction Date: 20221128. Publication Type: Journal (0100), Peer Reviewed Journal (0110). Format Covered: Electronic. Document Type: Journal Article. Language: EnglishMajor Descriptor: Awareness; Classrooms; Education; Technology; Augmented Reality. Minor Descriptor: Behavior Change; Facilities; Science Education. Classification: Educational & School Psychology (3500). Population: Human (10). Location: US. Age Group: Childhood (birth-12 yrs) (100); School Age (6-12 yrs) (180); Adolescence (13-17 yrs) (200). Methodology: Empirical Study; Quantitative Study. References Available: Y. Page Count: 11. Issue Publication Date: Jun, 2019. Publication History: First Posted Date: Apr 1, 2019; Accepted Date: Mar 24, 2019; Revised Date: Mar 13, 2019; First Submitted Date: Oct 31, 2018. Copyright Statement: All rights reserved. Elsevier B.V. 2019. 
AB  - The use of maker community tools and IoT technologies inside classrooms is spreading to an ever-increasing number of education and science fields. GAIA is a European research project focused on achieving behavior change for sustainability and energy awareness in schools. In this work, we report on how a large IoT deployment in a number of educational buildings and real-world data from this infrastructure, are utilized to support a 'maker' lab kit activity inside the classroom. We also provide some insights to the integration of these activities in the school curriculum, along with a discussion on feedback produced through a series of workshop activities in a number of schools in Greece. Moreover, we discuss the application of the lab kit framework towards implementing an interactive installation. We also report on how the lab kit is paired with a serious game and an augmented reality app for smartphones and tablets, supporting the in-class activities. Our initial evaluation results show a very positive first reaction by the school community. (PsycInfo Database Record (c) 2022 APA, all rights reserved)
KW  - Internet of things
KW  - Energy awareness
KW  - Gamification
KW  - Empirical studies
KW  - Sustainability
KW  - Augmented reality
KW  - Awareness
KW  - Classrooms
KW  - Education
KW  - Technology
KW  - Augmented Reality
KW  - Behavior Change
KW  - Facilities
KW  - Science Education
U1  - Sponsor: European Union, Europe. Other Details: Research project ‘‘Green Awareness In Action’’. Recipients: No recipient indicated
U1  - Sponsor: European Commission, Europe. Grant: 696029. Other Details: EASME under H2020. Recipients: No recipient indicated
DO  - 10.1016/j.ijcci.2019.03.003
UR  - https://login.ezproxy.elib11.ub.unimaas.nl/login?url=https://search.ebscohost.com/login.aspx?direct=true&db=psyh&AN=2019-37840-006&site=ehost-live&scope=site
UR  - ORCID: 0000-0001-9938-6211
UR  - ORCID: 0000-0003-2128-720X
UR  - mylonasg@cti.gr
DP  - EBSCOhost
DB  - psyh
ER  - 

TY  - THES
ID  - 2013-99090-594
AN  - 2013-99090-594
AU  - Ahmed, Ahmed Ahmed Abdalwhab Sayed
T1  - A framework for interoperability of virtual reality and simulation models in support of training
JF  - Dissertation Abstracts International Section A: Humanities and Social Sciences
JO  - Dissertation Abstracts International Section A: Humanities and Social Sciences
Y1  - 2013///
VL  - 73
IS  - 11-A(E)
PB  - ProQuest Information & Learning
SN  - 0419-4209
SN  - 978-0-494-87752-4
N1  - Accession Number: 2013-99090-594. Other Journal Title: Dissertation Abstracts International. Partial author list: First Author & Affiliation: Ahmed, Ahmed Ahmed Abdalwhab Sayed; Carleton U., Canada. Release Date: 20130701. Correction Date: 20221128. Publication Type: Dissertation Abstract (0400). Format Covered: Electronic. Document Type: Dissertation. Dissertation Number: AAINR87752. ISBN: 978-0-494-87752-4. Language: EnglishMajor Descriptor: Algorithms; Computer Programming; Computer Simulation; Training; Virtual Reality. Minor Descriptor: Decision Making. Classification: Educational & School Psychology (3500). Population: Human (10). Methodology: Empirical Study; Quantitative Study; Scientific Simulation. 
AB  - Computer-based Modeling and Simulation (M&S) is a powerful tool for cost-effective analysis, design, control, and optimization of complex dynamic systems. One of the most advanced general purpose (M&S) frameworks is the Discrete Event System Specification (DEVS) formalism. Cell-DEVS combines DEVS with cellular models, allowing complex systems to be described using simple rules. 3D visualization methods (such as virtual reality (VR) or computer games) provide support to simulation studies; however, at present there is no way to integrate 3D visualization for DEVS and Cell-DEVS (using interactive, collaborative platforms). Likewise, developing and modifying scenarios in existing VR environments usually requires significant efforts in programming and validation. In order to solve the aforementioned problems, this thesis focuses on the definition, design, and performance analysis of the visual Cell-DEVS (VCELL) framework, which allows different simulation models to receive real-time data to interact, collaborate, and adapt to simulation events (integrating 3D visualization, sensor data, DEVS, and Cell-DEVS modeling and simulation), which improves the models' design. As a proof of concept, we applied VCELL to different applications, including building information modeling (BIM), emergency and disaster simulation, and land combat simulation. BIM is used to generate and manage data for buildings during the project life cycle. Existing BIM applications include models for indoor climate, energy consumption, and CO2 emissions. However, they do not take into consideration other problems. This research shows a more generic environment for Cell-DEVS and BIM integration, and a prototype implementation in the form of BIM for Cell-DEVS simulation and visualization. In emergency and disaster simulations, it is usually important to consider the system evolution in time and space. Generally, such simulations are large-scale programs, which in turn raise the need for efficient simulation engines. However, some of these emergency simulations do not have real-time input data and are not adaptive. VCELL solves these problems, allowing the emergency simulation to be integrated with 3D visualization in real time. In the area of land combat simulation, agent-based distillation (ABD) provides a method for studying different land combat behaviors, which helps with decision-making. ABD movement algorithms are used to simulate the target's movement in the battlefield by a large set of parameters. However, under some circumstances, these movement algorithms use random movements, which may result in an unpredicted simulation output. We propose to use a model that integrates a cellular agent model and a collaborative 3D visual agent model in real time. VCELL allows the randomization problem to be solved, providing more stable output and reducing the scenario development, modification, and validation time. The main ideas, design, and implementation of VCELL are discussed, and the case studies are presented in detail. (PsycInfo Database Record (c) 2022 APA, all rights reserved)
KW  - interoperability
KW  - virtual reality
KW  - simulation models
KW  - training support
KW  - computer programming
KW  - decision making
KW  - algorithms
KW  - Algorithms
KW  - Computer Programming
KW  - Computer Simulation
KW  - Training
KW  - Virtual Reality
KW  - Decision Making
UR  - https://login.ezproxy.elib11.ub.unimaas.nl/login?url=https://search.ebscohost.com/login.aspx?direct=true&db=psyh&AN=2013-99090-594&site=ehost-live&scope=site
DP  - EBSCOhost
DB  - psyh
ER  - 

TY  - JOUR
ID  - 2008-16484-002
AN  - 2008-16484-002
AU  - Lin, Chyi-Yeu
AU  - Tseng, Chang-Kuo
AU  - Jo, Po-Chia
T1  - A multi-functional entertaining and educational robot
JF  - Journal of Intelligent & Robotic Systems
JO  - Journal of Intelligent & Robotic Systems
JA  - J Intell Robot Syst
Y1  - 2008/12//
VL  - 53
IS  - 4
SP  - 299
EP  - 330
PB  - Springer
SN  - 0921-0296
SN  - 1573-0409
AD  - Lin, Chyi-Yeu, Department of Mechanical Engineering, National Taiwan University of Science and Technology, 43 Keelung Road, Section 4, Taipei, Taiwan, 10672
N1  - Accession Number: 2008-16484-002. Partial author list: First Author & Affiliation: Lin, Chyi-Yeu; Department of Mechanical Engineering, National Taiwan University of Science and Technology, Taipei, Taiwan. Release Date: 20090316. Correction Date: 20221128. Publication Type: Journal (0100), Peer Reviewed Journal (0110). Format Covered: Electronic. Document Type: Journal Article. Language: EnglishMajor Descriptor: Artificial Intelligence; Human Computer Interaction; Robotics. Classification: Cognitive Psychology & Intelligent Systems (4100). Population: Human (10). References Available: Y. Page Count: 32. Issue Publication Date: Dec, 2008. 
AB  - This paper presents a multi-functional autonomous intelligent robot, DOC-1, which has actions exclusively driven by artificial intelligence programs. The mechanism of this robot was designed to fulfill tasks defined by various functions such as gripping character cubes and teacups, playing the Gobang board game, and rotating and stacking character cubes. Further emphasis was placed on load lifting capability, weight reduction, energy conservation, and performance reliability. The serial port of a minicomputer is used as the communication interface between the software and electromechanical components. A custom-made chip serves as the control kernel that controls the motions of servomotors that move the arms and head of the robot, two DC motors which drive the wheels, and also a number of lights. With the integrated artificial intelligence software and the robot control system, this intelligent robot DOC-1 can perform a number of autonomous functions that make it interactive with human beings. (PsycInfo Database Record (c) 2022 APA, all rights reserved)
KW  - multi-functional autonomous intelligent robot
KW  - artificial intelligence programs
KW  - minicomputer
KW  - communication interface
KW  - DOC-1
KW  - Artificial Intelligence
KW  - Human Computer Interaction
KW  - Robotics
DO  - 10.1007/s10846-008-9241-6
UR  - https://login.ezproxy.elib11.ub.unimaas.nl/login?url=https://search.ebscohost.com/login.aspx?direct=true&db=psyh&AN=2008-16484-002&site=ehost-live&scope=site
UR  - jerrylin@mail.ntust.edu.tw
DP  - EBSCOhost
DB  - psyh
ER  - 
